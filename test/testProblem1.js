const {createRandomJsonFiles,deleteDirectory} = require('../problem1')

const dirPath = 'fsCallbacksAsyncAwait/myDir';
const dirName = 'myDir'

async function main() {
    try {
      await createRandomJsonFiles(dirPath);
      
      console.log(`Created directory ${dirPath} and files`);
  
      await deleteDirectory(dirPath);
      console.log(`Deleted directory ${dirName} and files`);
    } catch (err) {
      console.error(`Error: ${err}`);
    }
  }
  
  main();
  