const { writeFile, readFile, appendFile, unlink } = require('../problem2')


async function problem2() {
    try {
        const fileContent = await readFile('fsCallbacksAsyncAwait/lipusum.txt');
        const upperContent = fileContent.toUpperCase();
        const upperFileName = 'fsCallbacksAsyncAwait/upper.txt';
        await writeFile(upperFileName, upperContent);
        await appendFile('fsCallbacksAsyncAwait/filenames.txt', `${upperFileName}\n`);

        const lowerContent = upperContent.toLowerCase();
        const sentences = lowerContent.split(/[.!?]/);
        const lowerFileName = 'fsCallbacksAsyncAwait/lower.txt';
        await writeFile(lowerFileName, sentences.join('\n'));
        await appendFile('fsCallbacksAsyncAwait/filenames.txt', `${lowerFileName}\n`);

        const sortedContent = sentences.sort().join('\n');
        const sortedFileName = 'fsCallbacksAsyncAwait/sorted.txt';
        await writeFile(sortedFileName, sortedContent);
        await appendFile('fsCallbacksAsyncAwait/filenames.txt', `${sortedFileName}\n`);

        const nameOfFiles = await readFile('fsCallbacksAsyncAwait/filenames.txt');
        const fileNames = nameOfFiles.trim().split('\n');
        const unlinkPromises = fileNames.map(async (fileName) => {
            await unlink(fileName);
            console.log(`Deleted ${fileName}`);
        });
        await Promise.all(unlinkPromises);
    } catch (err) {
        console.error(`Error: ${err}`);
    }
}

problem2()