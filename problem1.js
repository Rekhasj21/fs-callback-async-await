const fs = require('fs');
const path = require('path');

const numOfFiles = 5;

async function mkdirAsync(dir) {
    return new Promise((resolve, reject) => {
        fs.mkdir(dir, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

async function writeFileAsync(filePath, fileContent) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, fileContent, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

async function readdirAsync(dir) {
    return new Promise((resolve, reject) => {
        fs.readdir(dir, (err, files) => {
            if (err) {
                reject(err);
            } else {
                resolve(files);
            }
        });
    });
}

async function unlinkAsync(filePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

async function rmdirAsync(dir) {
    return new Promise((resolve, reject) => {
        fs.rmdir(dir, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

async function createRandomJsonFiles(dir) {
    await mkdirAsync(dir);
    for (let i = 1; i <= numOfFiles; i++) {
        const fileName = `file_${Math.ceil(Math.random() * 100)}.json`;
        const filePath = path.join(dir, fileName);
        const fileContent = JSON.stringify({ message: `This is file ${i}` });
        console.log(`created ${fileName}`);
        await writeFileAsync(filePath, fileContent);
    }
}

async function deleteDirectory(dir) {
    let files;
    try {
        files = await readdirAsync(dir);
    }
    catch (err) {
        console.log(err)
    }
    const promises = files.map(async (file) => {
        const filePath = path.join(dir, file);
        await unlinkAsync(filePath);
    });
    await Promise.all(promises);
    await rmdirAsync(dir);
}

module.exports = { createRandomJsonFiles, deleteDirectory }